import math
import torch
from PIL import Image
from io import BytesIO
from torchvision import transforms
from pytorch_msssim import ms_ssim
from compressai.zoo import bmshj2018_hyperprior, mbt2018

device = 'cuda' if torch.cuda.is_available() else 'cpu'

bmshj2 = bmshj2018_hyperprior(quality=2, pretrained=True).eval().to(device)
bmshj7 = bmshj2018_hyperprior(quality=7, pretrained=True).eval().to(device)
bmshj2.eval()
bmshj7.eval()

mbt2 = mbt2018(quality=2, pretrained=True).eval().to(device)
mbt7 = mbt2018(quality=7, pretrained=True).eval().to(device)
mbt2.eval()
mbt7.eval()

model = {'best':[bmshj7, mbt7], 'normal':[bmshj2, mbt2]}
name = ["bmshj", "mbt"]

def processImage(selectedImage, quality):
    img = Image.open(selectedImage).convert('RGB')
    x = transforms.ToTensor()(img).unsqueeze(0).to(device)
    x = transforms.Resize([768,512])(x)
    
    out_net = {}
    modelList = model[quality]
    with torch.no_grad():
        for i in range(len(modelList)):
            out_net[name[i]] = modelList[i].forward(x)
            out_net[name[i]]['x_hat'].clamp_(0, 1)
    out_net['originalDim'] = img.size
    return (x, out_net)

def getMetrics(a, b):
    psnr = compute_psnr(a, b["x_hat"])
    msssim = compute_msssim(a, b["x_hat"])
    bpp = compute_bpp(b)
    size = compressSize(b['x_hat'])
    metrics = [psnr,msssim,bpp, size]
    return metrics

def compute_psnr(a, b):
    mse = torch.mean((a - b)**2).item()
    return -10 * math.log10(mse)

def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1.).item()

def compute_bpp(out_net):
    size = out_net['x_hat'].size()
    num_pixels = size[0] * size[2] * size[3]
    return sum(torch.log(likelihoods).sum() / (-math.log(2) * num_pixels)
              for likelihoods in out_net['likelihoods'].values()).item()

def compressSize(b):
    rec_net = transforms.ToPILImage()(b.squeeze().cpu())
    temp = BytesIO()
    rec_net.save(temp, 'JPEG', quality=70)
    temp.seek(0)
    compressSize = temp.getbuffer().nbytes
    return compressSize/1024