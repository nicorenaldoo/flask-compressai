a
    Vy�`�V  �                   @   s�   d dl Z d dlZd dlZd dlmZ d dlm  mZ d dlm	Z	m
Z
 d dlmZmZ d dlmZmZ ddlmZmZmZ g d�ZG dd	� d	ej�ZG d
d� de�ZdZdZdZeeefdd�ZG dd� de�ZG dd� de�ZG dd� de�ZdS )�    N)�BufferedRansEncoder�RansDecoder)�EntropyBottleneck�GaussianConditional)�GDN�MaskedConv2d�   )�conv�deconv�update_registered_buffers)�CompressionModel�FactorizedPrior�ScaleHyperprior�MeanScaleHyperprior�%JointAutoregressiveHierarchicalPriorsc                       sP   e Zd ZdZd� fdd�	Zdd� Zdd� Zd	d
� Zddd�Z� fdd�Z	�  Z
S )r   z�Base class for constructing an auto-encoder with at least one entropy
    bottleneck module.

    Args:
        entropy_bottleneck_channels (int): Number of channels of the entropy
            bottleneck
    Tc                    s$   t � ��  t|�| _|r | ��  d S �N)�super�__init__r   �entropy_bottleneck�_initialize_weights)�self�entropy_bottleneck_channels�init_weights��	__class__� �lc:\nico\randomstuff\programming project\tpid\compressai-master\compressai-master\compressai\models\priors.pyr   .   s    

zCompressionModel.__init__c                 C   s   t dd� | �� D ��}|S )z\Return the aggregated loss over the auxiliary entropy bottleneck
        module(s).
        c                 s   s    | ]}t |t�r|�� V  qd S r   )�
isinstancer   �loss)�.0�mr   r   r   �	<genexpr>9   s   z,CompressionModel.aux_loss.<locals>.<genexpr>)�sum�modules)r   �aux_lossr   r   r   r$   5   s    �zCompressionModel.aux_lossc                 C   sJ   | � � D ]<}t|tjtjf�rtj�|j� |jd urtj�	|j� qd S r   )
r#   r   �nn�Conv2d�ConvTranspose2d�init�kaiming_normal_�weight�bias�zeros_)r   r    r   r   r   r   >   s
    
z$CompressionModel._initialize_weightsc                 G   s
   t � �d S r   )�NotImplementedError)r   �argsr   r   r   �forwardE   s    zCompressionModel.forwardFc                 C   s6   d}| � � D ]$}t|t�sq|j|d�}||O }q|S )am  Updates the entropy bottleneck(s) CDF values.

        Needs to be called once after training to be able to later perform the
        evaluation with an actual entropy coder.

        Args:
            force (bool): overwrite previous values (default: False)

        Returns:
            updated (bool): True if one of the EntropyBottlenecks was updated.

        F��force)�childrenr   r   �update)r   r1   �updatedr    �rvr   r   r   r3   H   s    

zCompressionModel.updatec                    s$   t | jdg d�|� t� �|� d S )Nr   )�_quantized_cdf�_offset�_cdf_length)r   r   r   �load_state_dict�r   �
state_dictr   r   r   r9   ]   s    �z CompressionModel.load_state_dict)T)F)�__name__�
__module__�__qualname__�__doc__r   r$   r   r/   r3   r9   �__classcell__r   r   r   r   r   %   s   	
r   c                       sV   e Zd ZdZ� fdd�Zeed�dd��Zdd� Ze	d	d
� �Z
dd� Zdd� Z�  ZS )r   a�  Factorized Prior model from J. Balle, D. Minnen, S. Singh, S.J. Hwang,
    N. Johnston: `"Variational Image Compression with a Scale Hyperprior"
    <https://arxiv.org/abs/1802.01436>`_, Int Conf. on Learning Representations
    (ICLR), 2018.

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    c                    s�   t � jf d|i|�� t�td|�t|�t||�t|�t||�t|�t||��| _t�t||�t|dd�t||�t|dd�t||�t|dd�t|d��| _|| _	|| _
d S )Nr   �   T��inverse)r   r   r%   �
Sequentialr	   r   �g_ar
   �g_s�N�M�r   rG   rH   �kwargsr   r   r   r   t   s*    �



�
zFactorizedPrior.__init__��returnc                 C   s   dS )N�   r   �r   r   r   r   �downsampling_factor�   s    z#FactorizedPrior.downsampling_factorc                 C   s0   | � |�}| �|�\}}| �|�}|d|id�S )N�y��x_hat�likelihoods)rE   r   rF   )r   �xrP   �y_hat�y_likelihoodsrR   r   r   r   r/   �   s    

��zFactorizedPrior.forwardc                 C   s4   |d � d�}|d � d�}| ||�}|�|� |S �z.Return a new model instance from `state_dict`.zg_a.0.weightr   zg_a.6.weight��sizer9   ��clsr;   rG   rH   �netr   r   r   �from_state_dict�   s
    

zFactorizedPrior.from_state_dictc                 C   s.   | � |�}| j�|�}|g|�� dd � d�S �N�������strings�shape)rE   r   �compressrY   )r   rT   rP   �	y_stringsr   r   r   rc   �   s    
zFactorizedPrior.compressc                 C   sF   t |t�rt|�dksJ �| j�|d |�}| �|��dd�}d|iS )Nr   r   rR   )r   �list�lenr   �
decompressrF   �clamp_)r   ra   rb   rU   rR   r   r   r   rg   �   s    zFactorizedPrior.decompress)r<   r=   r>   r?   r   �property�intrO   r/   �classmethodr]   rc   rg   r@   r   r   r   r   r   h   s   
r   g)\���(�?�   �@   c                 C   s    t �t �t�| �t�|�|��S r   )�torch�exp�linspace�math�log)�min�max�levelsr   r   r   �get_scale_table�   s    rv   c                       sp   e Zd ZdZ� fdd�Zeed�dd��Zdd� Z� fd	d
�Z	e
dd� �Zd� fdd�	Zdd� Zdd� Z�  ZS )r   a�  Scale Hyperprior model from J. Balle, D. Minnen, S. Singh, S.J. Hwang,
    N. Johnston: `"Variational Image Compression with a Scale Hyperprior"
    <https://arxiv.org/abs/1802.01436>`_ Int. Conf. on Learning Representations
    (ICLR), 2018.

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    c                    s>  t � jf d|i|�� t�td|�t|�t||�t|�t||�t|�t||��| _t�t||�t|dd�t||�t|dd�t||�t|dd�t|d��| _t�t||ddd�tj	dd�t||�tj	dd�t||��| _
t�t||�tj	dd�t||�tj	dd�t||ddd�tj	dd��| _td �| _t|�| _t|�| _d S )Nr   rA   TrB   r   ��stride�kernel_size��inplace)r   r   r%   rD   r	   r   rE   r
   rF   �ReLU�h_a�h_sr   �gaussian_conditionalrj   rG   rH   rI   r   r   r   r   �   sJ    �



�


�


�	

zScaleHyperprior.__init__rK   c                 C   s   dS �Nrm   r   rN   r   r   r   rO   �   s    z#ScaleHyperprior.downsampling_factorc           
      C   s\   | � |�}| �t�|��}| �|�\}}| �|�}| �||�\}}| �|�}	|	||d�d�S )N�rP   �zrQ   )rE   r}   rn   �absr   r~   r   rF   )
r   rT   rP   r�   �z_hat�z_likelihoods�
scales_hatrU   rV   rR   r   r   r   r/   �   s    


�zScaleHyperprior.forwardc                    s$   t | jdg d�|� t� �|� d S )Nr   )r6   r7   r8   �scale_table)r   r   r   r9   r:   r   r   r   r9     s    �zScaleHyperprior.load_state_dictc                 C   s4   |d � d�}|d � d�}| ||�}|�|� |S rW   rX   rZ   r   r   r   r]     s
    

zScaleHyperprior.from_state_dictNFc                    s4   |d u rt � }| jj||d�}|t� j|d�O }|S )Nr0   )rv   r   �update_scale_tabler   r3   )r   r�   r1   r4   r   r   r   r3     s
    zScaleHyperprior.updatec           	      C   s~   | � |�}| �t�|��}| j�|�}| j�||�� dd � �}| �|�}| j	�
|�}| j	�||�}||g|�� dd � d�S r^   )rE   r}   rn   r�   r   rc   rg   rY   r~   r   �build_indexes)	r   rT   rP   r�   �	z_stringsr�   r�   �indexesrd   r   r   r   rc     s    

zScaleHyperprior.compressc                 C   sn   t |t�rt|�dksJ �| j�|d |�}| �|�}| j�|�}| j�|d |�}| �|��	dd�}d|iS )N�   r   r   rR   )
r   re   rf   r   rg   r~   r   r�   rF   rh   )r   ra   rb   r�   r�   r�   rU   rR   r   r   r   rg   +  s    
zScaleHyperprior.decompress)NF)r<   r=   r>   r?   r   ri   rj   rO   r/   r9   rk   r]   r3   rc   rg   r@   r   r   r   r   r   �   s   ,	
r   c                       s8   e Zd ZdZ� fdd�Zdd� Zdd� Zdd	� Z�  ZS )
r   a�  Scale Hyperprior with non zero-mean Gaussian conditionals from D.
    Minnen, J. Balle, G.D. Toderici: `"Joint Autoregressive and Hierarchical
    Priors for Learned Image Compression" <https://arxiv.org/abs/1809.02736>`_,
    Adv. in Neural Information Processing Systems 31 (NeurIPS 2018).

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    c                    s�   t � j||fi |�� t�t||ddd�tjdd�t||�tjdd�t||��| _t�t||�tjdd�t||d d �tjdd�t|d d |d ddd��| _d S )Nr   rA   rw   Trz   r�   )	r   r   r%   rD   r	   �	LeakyReLUr}   r
   r~   rI   r   r   r   r   A  s    

�

�zMeanScaleHyperprior.__init__c                 C   sj   | � |�}| �|�}| �|�\}}| �|�}|�dd�\}}| j|||d�\}	}
| �|	�}||
|d�d�S )Nr�   r   ��meansr�   rQ   )rE   r}   r   r~   �chunkr   rF   )r   rT   rP   r�   r�   r�   �gaussian_paramsr�   �	means_hatrU   rV   rR   r   r   r   r/   T  s    



�zMeanScaleHyperprior.forwardc                 C   s�   | � |�}| �|�}| j�|�}| j�||�� dd � �}| �|�}|�dd�\}}| j�	|�}	| jj||	|d�}
|
|g|�� dd � d�S )Nr_   r�   r   r�   r`   )
rE   r}   r   rc   rg   rY   r~   r�   r   r�   )r   rT   rP   r�   r�   r�   r�   r�   r�   r�   rd   r   r   r   rc   b  s    


zMeanScaleHyperprior.compressc           
      C   s�   t |t�rt|�dksJ �| j�|d |�}| �|�}|�dd�\}}| j�|�}| jj|d ||d�}| �	|��
dd�}	d|	iS )Nr�   r   r   r�   rR   )r   re   rf   r   rg   r~   r�   r   r�   rF   rh   )
r   ra   rb   r�   r�   r�   r�   r�   rU   rR   r   r   r   rg   o  s    

�zMeanScaleHyperprior.decompress)	r<   r=   r>   r?   r   r/   rc   rg   r@   r   r   r   r   r   5  s
   r   c                       sh   e Zd ZdZd� fdd�	Zeed�dd��Zdd	� Ze	d
d� �Z
dd� Zdd� Zdd� Zdd� Z�  ZS )r   a�  Joint Autoregressive Hierarchical Priors model from D.
    Minnen, J. Balle, G.D. Toderici: `"Joint Autoregressive and Hierarchical
    Priors for Learned Image Compression" <https://arxiv.org/abs/1809.02736>`_,
    Adv. in Neural Information Processing Systems 31 (NeurIPS 2018).

    Args:
        N (int): Number of channels
        M (int): Number of channels in the expansion layers (last layer of the
            encoder and last layer of the hyperprior decoder)
    ��   c                    s  t � jf ||d�|�� t�td|ddd�t|�t||ddd�t|�t||ddd�t|�t||ddd��| _t�t||ddd�t|dd�t||ddd�t|dd�t||ddd�t|dd�t|dddd��| _t�t||ddd	�tj	dd
�t||ddd	�tj	dd
�t||ddd	��| _
t�t||ddd	�tj	dd
�t||d d ddd	�tj	dd
�t|d d |d ddd	��| _t�t�|d d |d d d�tj	dd
�t�|d d |d d d�tj	dd
�t�|d d |d d d��| _t|d| dddd�| _td �| _t|�| _t|�| _d S )N)rG   rH   rA   �   r�   )ry   rx   TrB   r   rw   rz   �   �
   �   �   )ry   �paddingrx   )r   r   r%   rD   r	   r   rE   r
   rF   r�   r}   r~   r&   �entropy_parametersr   �context_predictionr   r   rj   rG   rH   rI   r   r   r   r   �  s\    �



�


�

�

��

z.JointAutoregressiveHierarchicalPriors.__init__rK   c                 C   s   dS r�   r   rN   r   r   r   rO   �  s    z9JointAutoregressiveHierarchicalPriors.downsampling_factorc                 C   s�   | � |�}| �|�}| �|�\}}| �|�}| j�|| jr>dnd�}| �|�}| �t	j
||fdd��}	|	�dd�\}
}| j||
|d�\}}| �|�}|||d�d�S )	N�noise�
dequantizer   ��dimr�   r�   r�   rQ   )rE   r}   r   r~   r   �quantize�trainingr�   r�   rn   �catr�   rF   )r   rT   rP   r�   r�   r�   �paramsrU   Z
ctx_paramsr�   r�   r�   �_rV   rR   r   r   r   r/   �  s"    


�
�
�z-JointAutoregressiveHierarchicalPriors.forwardc                 C   s4   |d � d�}|d � d�}| ||�}|�|� |S rW   rX   rZ   r   r   r   r]   �  s
    

z5JointAutoregressiveHierarchicalPriors.from_state_dictc              	   C   s  t | �� �jt�d�kr"t�d� | �|�}| �|�}| j�	|�}| j�
||�� dd � �}| �|�}d}d}|d d }	|�d�| }
|�d�| }t�||	|	|	|	f�}g }t|�d	��D ]:}| �|||d � |||d � |
|||	�}|�|� q�||g|�� dd � d
�S )N�cpu�qInference on GPU is not recommended for the autoregressive models (the entropy coder is run sequentially on CPU).r_   �   r�   r   r�   rA   r   r`   )�next�
parameters�devicern   �warnings�warnrE   r}   r   rc   rg   rY   r~   �F�pad�range�_compress_ar�append)r   rT   rP   r�   r�   r�   r�   �sry   r�   �y_height�y_widthrU   rd   �i�stringr   r   r   rc   �  s6    �


�z.JointAutoregressiveHierarchicalPriors.compressc              	   C   s�  | j j�� }| j j�� }| j j�� }	t� }
g }g }| jj| jj }t	|�D �],}t	|�D �]}|d d �d d �||| �||| �f }t
j||| jjd�}|d d �d d �||d �||d �f }| �tj||fdd��}|�d��d�}|�dd�\}}| j �|�}|d d �d d �||f }| j �|d|�}|| |d d �d d �|| || f< |�|�� �� � |�|�� �� � qXqJ|
�|||||	� |
�� }|S )N�r+   r   r�   rA   r�   �symbols)r   �quantized_cdf�tolist�
cdf_length�offsetr   r�   r*   �maskr�   r�   �conv2dr+   r�   rn   r�   �squeezer�   r�   r�   �extend�encode_with_indexes�flush)r   rU   r�   �height�widthry   r�   �cdf�cdf_lengths�offsets�encoderZsymbols_listZindexes_listZmasked_weight�h�w�y_crop�ctx_p�pr�   r�   r�   r�   Zy_qr�   r   r   r   r�     s<    (�($
�z2JointAutoregressiveHierarchicalPriors._compress_arc              
   C   s2  t |t�rt|�dksJ �t| �� �jt�d�kr<t�d� | j	�
|d |�}| �|�}d}d}|d d }|�d�| }|�d�| }	tj|�d�| j|d|  |	d|  f|jd	�}
t|d �D ]6\}}| �||
||d � |||d � ||	||� q�t�|
| | | | f�}
| �|
��dd�}d
|iS )Nr�   r�   r�   r   r�   r�   rA   r   )r�   rR   )r   re   rf   r�   r�   r�   rn   r�   r�   r   rg   r~   rY   �zerosrH   �	enumerate�_decompress_arr�   r�   rF   rh   )r   ra   rb   r�   r�   r�   ry   r�   r�   r�   rU   r�   �y_stringrR   r   r   r   rg   4  s:    �
"��
z0JointAutoregressiveHierarchicalPriors.decompressc              
   C   sh  | j j�� }| j j�� }	| j j�� }
t� }|�|� t|�D �]$}t|�D �]}|d d �d d �||| �||| �f }tj	|| j
j| j
jd�}|d d �d d �||d �||d �f }| �tj||fdd��}|�dd�\}}| j �|�}|�|�� �� ||	|
�}t�|��dddd�}| j �||�}|| }|| }||d d �d d �||d �||d �f< qJq<d S )Nr�   r   r�   r�   �����)r   r�   r�   r�   r�   r   �
set_streamr�   r�   r�   r�   r*   r+   r�   rn   r�   r�   r�   Zdecode_streamr�   �Tensor�reshaper�   )r   r�   rU   r�   r�   r�   ry   r�   r�   r�   r�   �decoderr�   r�   r�   r�   r�   r�   r�   r�   r�   r5   �hp�wpr   r   r   r�   `  s2    
(�(�z4JointAutoregressiveHierarchicalPriors._decompress_ar)r�   r�   )r<   r=   r>   r?   r   ri   rj   rO   r/   rk   r]   rc   r�   rg   r�   r@   r   r   r   r   r   |  s   7
&,,r   ) rq   r�   rn   �torch.nnr%   Ztorch.nn.functional�
functionalr�   Zcompressai.ansr   r   Zcompressai.entropy_modelsr   r   Zcompressai.layersr   r   �utilsr	   r
   r   �__all__�Moduler   r   Z
SCALES_MINZ
SCALES_MAXZSCALES_LEVELSrv   r   r   r   r   r   r   r   �<module>   s$   	CLxG