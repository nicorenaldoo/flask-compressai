import os
from io import BytesIO
from torchvision import transforms
from tempfile import NamedTemporaryFile
from compress import processImage, getMetrics
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask import Flask, render_template, request, send_from_directory, send_file

app = Flask(__name__, template_folder='./src/templates')

photos = UploadSet('photos', IMAGES)
imgPath = app.config['UPLOADED_PHOTOS_DEST'] = './src/img/'
configure_uploads(app, photos)

@app.route('/', methods=['GET', 'POST'])
def home():
    listdir = os.listdir(app.config['UPLOADED_PHOTOS_DEST'])
    if request.method == 'POST':
        file = request.files['photo']
        if file.filename != '':
            filepath = photos.save(file)
        else:
            filepath = request.form.get('filepath')
        global output
        fullpath = imgPath + filepath
        quality = request.form.get('quality')

        original, output = processImage(fullpath, quality)
        metrics_bmshj = getMetrics(original, output["bmshj"])
        metrics_mbt = getMetrics(original, output["mbt"])
        context = {
            'listdir': listdir,
            'predictedImage' : filepath,
            'filesize': os.stat(fullpath).st_size/1024,

            "bmshj" : metrics_bmshj,
            "mbt" : metrics_mbt
        }
        return render_template("index.html", **context)
    return render_template('index.html', listdir=listdir)

@app.route('/image/<filename>')
def display_image(filename=''):
    return send_from_directory(app.config["UPLOADED_PHOTOS_DEST"], filename)

# @app.route('/tif/')
# def display_tif():
#     from skimage.io import imread
#     from skimage.color import gray2rgb
#     import matplotlib.pyplot as plt
#     img = imread('./src/img/55.jpeg')
#     original, output = processImage('./src/img/55.jpeg')
#     rec_net = transforms.ToPILImage()(output["mbt"]["x_hat"].squeeze().cpu())
#     plt.imshow(rec_net)
#     return render_template('index2.html', input = plt.show())

@app.route('/pil/<model>/<filename>')
def serve_pil_image(model, filename):
    tensor = output[model]['x_hat']
    rec_net = transforms.ToPILImage()(tensor.squeeze().cpu())
    rec_net = transforms.Resize([output['originalDim'][1],output['originalDim'][0]])(rec_net)
    img_io = BytesIO()
    rec_net.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpg')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)